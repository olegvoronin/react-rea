import React from 'react';
import Results from './Results';
import mock from 'json!../../mock.json';


export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = mock;
  }

  render() {
    const properties = this.state;

    return (
      <div className="rea-app">
        <main className="rea-results">
          <h3>Results 1&ndash;3 of 3</h3>
          <Results
            getButton={this.getButton}
            results={properties.results}
            onClick={this.saveProperty} />
        </main>
        <aside className="rea-saved-properties">
          <h3>Saved properties</h3>
          <Results
            getButton={this.getButton}
            results={properties.saved}
            onClick={this.deleteProperty} />
        </aside>
      </div>
    );
  }

  //hmm I wanted to see if 'computed' vue/knockout concept is doable
  //without orchestrating saving/unsaving events
  //I can see adding individual properties' isSaved to state
  //would be a cleaner, more react-ey alternative
  getButton = (_id) => {
    return () => {
      //return 'blah ' + this.state.saved.some(property => property.id === _id)
      return this.state.saved.some(property => property.id === _id) ?
        <button
          className="rea-button rea-button__delete"
          onClick={this.deleteProperty.bind(null, _id)}>
          ★ delete
        </button> :
        <button
          className="rea-button rea-button__save"
          onClick={this.saveProperty.bind(null, _id)}>
          ☆ save
        </button>
    }
  }

  deleteProperty = (_id, e) => {
    e.stopPropagation();

    this.setState({
      saved: this.state.saved.filter(property => property.id !== _id)
    });
  }

  saveProperty = (_id, e) => {
    e.stopPropagation();

    if (this.state.saved.some(property => property.id === _id)) {
      //already added
      return;
    }

    this.setState({
      saved: this.state.saved.concat(this.state.results.filter(property => property.id === _id))
    });
  }

  toggleProperty = (_id, e) => {
    e.stopPropagation();

    this.setState({
      saved: this.state.saved.some(property => property.id === _id) ?
        this.state.saved.filter(property => property.id !== _id) :
        this.state.saved.concat(this.state.results.filter(property => property.id === _id))
    });
  }

 
}