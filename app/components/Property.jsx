import React from 'react';


export default ({property, onClick, getButton = () => {return ''}}) => (
  <article className="rea-property">

    <header
      className="rea-property--header"
      style={{
        backgroundColor: property.agency.brandingColors.primary,
        backgroundImage: property.agency.logo }}>
      <img
        className="rea-property--logo"
        src={property.agency.logo} />
    </header>

    <img
      className="rea-property--main-image"
      src={property.mainImage} />

    {getButton()}

    <div className="rea-property--price">{property.price}</div>

  </article>
)