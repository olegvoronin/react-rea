import React from 'react';
import Property from './Property';


export default ({results, getButton, onClick=() => {}}) => (
  <div>
    {!results.length &&
      <p>No properties</p>
    }
    <ul>{results.map(_result =>
      <li key={_result.id}>
        <Property
          onClick={onClick.bind(null, _result.id)}
          getButton={getButton(_result.id)}
          property={_result} />
      </li>
    )}</ul>
  </div>
)