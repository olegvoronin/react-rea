import React from 'react';
import ReactTestUtils from 'react-dom/test-utils';
import assert from 'assert';
import Property from '../app/components/Property.jsx';
import mock from 'json!../mock.json';

describe('Property', () => {
  let reactComponent, property, onClick;

  before(() => {
    property = mock.results[0];
    onClick = () => {};

    reactComponent = ReactTestUtils.renderIntoDocument(
      <Wrapper>
        <Property property={property} />
      </Wrapper>
    );
  })

  it('renders price', () => {
    const priceComponent = ReactTestUtils.findRenderedDOMComponentWithClass(reactComponent, 'rea-property--price');
    
    assert.equal(priceComponent.textContent, property.price);
  });

  xit('renders using brand colours', () => {});
});

class Wrapper extends React.Component {
  render() {
    return <div>{this.props.children}</div>;
  }
}