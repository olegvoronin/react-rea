import React from 'react';
import ReactTestUtils from 'react-dom/test-utils';
import assert from 'assert';
import App from '../app/components/App.jsx';
import mock from 'json!../mock.json';


describe('App', () => {

  let reactComponent, property, onClick;

  before(() => {
    reactComponent = ReactTestUtils.renderIntoDocument(
      <App />
    );
  });

  it('removes property', () => {
    const deleteButton = ReactTestUtils.findRenderedDOMComponentWithClass(reactComponent, 'rea-button__delete');    

    ReactTestUtils.Simulate.click(deleteButton);

    assert.deepEqual(reactComponent.state.saved, []); //nice
  });

  xit('adds property', () => {});
  
  xit('doesnt add property twice', () => {});

});


class Wrapper extends React.Component {
  render() {
    return <div>{this.props.children}</div>;
  }
}
