# react-rea

First react sandbox, based on SurviveJS react-boilerplate

MIA:

* more testing
* sass, icons and, well, actual styling
* even more testing
* some sort of propertyStore I guess
* if only there was a more react-ey way of handling saved/not saved property state, perhaps something called state

Deployed to [olegvoronin.bitbucket.io/react-rea/](https://olegvoronin.bitbucket.io/react-rea/)

## Getting Started

1. `npm i` - Install dependencies. This might take a while.
2. `npm start` - Run development build. If it doesn't start, make sure you aren't running anything else in the same port 8888.
3. `npm run test` - Runs `tests/` through Karma/Phantom/Mocha once.
4. `npm run test:tdd` - Runs `tests/` in a TDD mode (watches for changes and rebuilds).